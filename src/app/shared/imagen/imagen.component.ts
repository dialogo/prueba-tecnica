import { Component, Input } from '@angular/core';
import { Imagen } from 'src/app/core/imagen/imagen.model';

@Component({
  selector: 'app-imagen',
  templateUrl: './imagen.component.html',
  styleUrls: ['./imagen.component.scss'],
})
export class ImagenComponent {

  @Input() item: Imagen;

}
