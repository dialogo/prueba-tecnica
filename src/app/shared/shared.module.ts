import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImagenComponent } from './imagen/imagen.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [ImagenComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports:[ImagenComponent]
})
export class SharedModule { }
