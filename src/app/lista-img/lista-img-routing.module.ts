import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaImgPage } from './lista-img.page';

const routes: Routes = [
  {
    path: '',
    component: ListaImgPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListaImgPageRoutingModule {}
