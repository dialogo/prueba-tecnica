import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListaImgPage } from './lista-img.page';

describe('ListaImgPage', () => {
  let component: ListaImgPage;
  let fixture: ComponentFixture<ListaImgPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaImgPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListaImgPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Cuando llamamos a getAllItems() y despues llama al getPhotoImg()',() => {

    it("push de arrays",fakeAsync(() => {
      /*component.allItems = [];
      component.items = [];
      component.totalRecords = 4000;
      component.maxPorCarga = 30;
      component.getAllItems();*/
      expect(component.allItems.length).toEqual(4000);
      expect(component.items.length).toEqual(30);
    }));

  });

  describe('Cuando llamamos a filter()',() => {

    it('con filtro', fakeAsync(() => {
      component.filtroText = "L";
      component.allItems = [
        {id:1,photo:"",text:"LOREM"},
        {id:2,photo:"",text:"L"},
        {id:3,photo:"",text:"OREM"}
      ];
      component.filter();
      expect(component.items.length).toBeGreaterThan(0);
      expect(component.itemsFiltrado.length).toBeGreaterThan(0);
      if(component.items.length === component.itemsFiltrado.length){
        expect(component.infiniteScroll.disabled).toBeTruthy();
      } else {
        expect(component.infiniteScroll.disabled).toBeFalsy();
      }
    }));

    it('sin filtro', fakeAsync(() => {
      component.filtroText = "";
      component.allItems = [
        {id:1,photo:"",text:"LOREM"},
        {id:2,photo:"",text:"L"},
        {id:3,photo:"",text:"OREM"}
      ];
      component.filter();
      expect(component.items.length).toBeGreaterThan(0);
      expect(component.itemsFiltrado.length).toEqual(0);
      expect(component.infiniteScroll.disabled).toBeFalsy();
    }));

  });

  describe('Llamada al infinite scroll -> loadData(event)',() => {

    it("con filtro", fakeAsync(() => {
      component.filtroText = "L";
      component.itemsFiltrado = [
        {id:1,photo:"",text:"LOREM"},
        {id:2,photo:"",text:"L"}
      ];
      component.loadData(null);
      tick(510);
      fixture.detectChanges();
    
      fixture.whenStable().then(() => {
        expect(component.items.length).toBeGreaterThan(0);
        if(component.items.length === component.itemsFiltrado.length){
          expect(component.infiniteScroll.disabled).toBeTruthy();
        }
      });
    }));

    it("sin filtro", fakeAsync(() => {
      component.filtroText = "";
      component.allItems = [
        {id:1,photo:"",text:"LOREM"},
        {id:2,photo:"",text:"L"},
        {id:3,photo:"",text:"OREM"}
      ];
      component.totalRecords = 3;
      component.loadData(null);
      tick(510);
      fixture.detectChanges();
    
      fixture.whenStable().then(() => {
        expect(component.items.length).toBeGreaterThan(0);
        if(component.items.length === component.totalRecords){
          expect(component.infiniteScroll.disabled).toBeTruthy();
        }
      });
    }));

  });
});


