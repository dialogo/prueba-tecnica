import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { Imagen } from '../core/imagen/imagen.model';
import { datosImagen } from '../core/imagen/imagen.const';

@Component({
  selector: 'app-lista-img',
  templateUrl: './lista-img.page.html',
  styleUrls: ['./lista-img.page.scss'],
})
export class ListaImgPage implements OnInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  allItems: Imagen[] = [];
  items: Imagen[] = [];
  itemsFiltrado: Imagen[] = [];
  filtroText: string = '';

  totalRecords: number = datosImagen.totalRecords;
  maxPorCarga: number = datosImagen.maxPorCarga;
  lorem: string = datosImagen.lorem;
  url: string = datosImagen.url;

  ngOnInit() {
    this.getAllItems();
  }

  getAllItems(){
    for (let i = 0; i < this.totalRecords; i++) {
      let idPhoto = Math.floor(Math.random()*1000+1);
      this.allItems.push({
        id: idPhoto,
        photo: this.url.replace('{{identificador}}',idPhoto.toString()),
        text: this.lorem.substring(0, Math.random() * (this.lorem.length - 100) + 100)
      });
    }
    this.getPhotoImg();
  }

  getPhotoImg(){
    let sizeItems = this.items.length;
    for (let i = sizeItems; i < this.maxPorCarga + sizeItems; i++) {
      if((this.filtroText && !this.itemsFiltrado[i]) || (!this.filtroText && !this.allItems[i])){ break;}
      this.items.push(this.filtroText ? this.itemsFiltrado[i] : this.allItems[i]);
    }
  }

  filtrado(){
    return this.allItems.filter( 
      item => item.id.toString().includes(this.filtroText) || item.text.includes(this.filtroText)
    );
  }

  loadData(event) {
    setTimeout(() => {
      this.getPhotoImg();
      if(event){event.target.complete()}
      let maxSize = this.filtroText ? this.itemsFiltrado.length : this.totalRecords;
      this.infiniteScroll.disabled = this.items.length === maxSize ? true : this.infiniteScroll.disabled;
    }, 500);
  }

  filter() {
    this.infiniteScroll.disabled = false;
    this.items = [];
    this.itemsFiltrado = this.filtroText ? this.filtrado() : [];
    this.getPhotoImg();
    if(this.items.length === this.itemsFiltrado.length) {this.infiniteScroll.disabled = true;}
  }
}
