import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListaImgPageRoutingModule } from './lista-img-routing.module';

import { ListaImgPage } from './lista-img.page';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    ListaImgPageRoutingModule
  ],
  declarations: [ListaImgPage]
})
export class ListaImgPageModule {}
